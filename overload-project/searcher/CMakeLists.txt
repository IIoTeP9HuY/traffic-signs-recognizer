add_executable(searcher searcher)
set_target_properties(searcher PROPERTIES
	COMPILE_FLAGS "-std=c++0x"
)

target_link_libraries(searcher ${GRAPE_COMMON_LIBRARIES})

