#include <cocaine/framework/logging.hpp>
#include <cocaine/framework/application.hpp>
#include <cocaine/framework/worker.hpp>
#include <cocaine/framework/services/urlfetcher.hpp>
#include <grape/elliptics_client_state.hpp>

#define BASE_URL "https://www.googleapis.com/youtube/v3/search?part=snippet" \
    "&q=%D1%82%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D0%B0%D1%8F%20%" \
    "D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C%20" \
    "%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE%D1%80%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%82%D0%BE%D1%80%D0%B0" \
    "&key=AIzaSyCZEBuLjK_9KLPHKYlLvdy-qWUb8tgI1oE"

#define NO_DATA "<no-data>"

using namespace ioremap::elliptics;

class app_context : public cocaine::framework::application<app_context>
{
public:
	// proxy to the logging service
	std::shared_ptr<cocaine::framework::logger_t> m_log;
    std::shared_ptr<cocaine::framework::urlfetcher_service_t> m_fetcher;

	// elliptics client generator
	elliptics_client_state m_state;

	app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager);
	void initialize();

    struct on_event :
            public cocaine::framework::handler<app_context>,
            public std::enable_shared_from_this<on_event>
    {
        on_event(std::shared_ptr<app_context> app) : cocaine::framework::handler<app_context>(app)
        {
            // pass
        }

        void on_chunk(const char *chunk, size_t size)
        {
            using namespace std::placeholders;

            exec_context context(data_pointer::copy(chunk, size));
            COCAINE_LOG_INFO(app()->m_log, "on_chunk: %s, data: \"%s\"", context.event(), context.data().to_string());

            std::string url = BASE_URL;
            std::string token = context.data().to_string();

            if (token != NO_DATA) {
                url += "&pageToken=";
                url += token;
            }

            auto future = app()->m_fetcher->get(url);
            future.on_message(std::bind(&on_event::on_donwload_finished_impl, shared_from_this(), _1));
            future.on_error(std::bind(&on_event::on_download_failed, shared_from_this(), _1, _2));
        }

        void on_donwload_finished_impl(const typename cocaine::io::event_traits<cocaine::io::urlfetch::get>::result_type &result)
        {
            on_donwload_finished(std::get<0>(result), std::get<1>(result), std::get<2>(result), std::get<3>(result));
        }

        void on_donwload_finished(bool success, const std::string &data, int code, const std::map<std::string, std::string> &)
        {
            if (!success) {
                on_error(code, cocaine::format("Error, http_code: %d", code));
                return;
            }

            std::string copy = data;
            auto begin = copy.begin();
            auto end = copy.end();
            auto new_end = end;

            for (;;) {
                new_end = std::remove(begin, end, '\n');
                if (new_end == end)
                    break;

                end = new_end;
            }

            COCAINE_LOG_INFO(app()->m_log, "Downloaded: success: %d, code: %d, data: \"%s\"", int(success), code, std::string()); //copy);
            {}

            rapidjson::Document reply_data;

            rapidjson::StringStream stream(data.c_str());

			reply_data.ParseStream<rapidjson::kParseDefaultFlags, rapidjson::UTF8<> >(stream);
			if (reply_data.HasParseError()) {
                std::string error_str = cocaine::format("Failed to parse youtube download: %s", reply_data.GetParseError());
                COCAINE_LOG_ERROR(app()->m_log, "%s", error_str);
                response()->error(EINVAL, error_str);
                return;
			}

            auto session = app()->m_state.create_session();

            if (reply_data.HasMember("nextPageToken")) {
                std::string nextPageToken = reply_data["nextPageToken"].GetString();

                COCAINE_LOG_INFO(app()->m_log, "Next page token: %s", nextPageToken);

                dnet_id id;
                id.group_id = 0;
                id.type = 0;
                session.transform(nextPageToken, id);

                session.exec(&id, "searcher-queue@push", nextPageToken);
            } else {
                COCAINE_LOG_INFO(app()->m_log, "No next-page token");
            }

            if (reply_data.HasMember("items")) {
                auto &items = reply_data["items"];

                for (rapidjson::SizeType i = 0; i < items.Size(); ++i) {
                    auto &object = items[i];

                    if (!object.HasMember("id"))
                        continue;

                    auto &objectId = object["id"];

                    if (!objectId.HasMember("videoId"))
                        continue;

                    std::string videoId = objectId["videoId"].GetString();
                    COCAINE_LOG_INFO(app()->m_log, "New videoId: %d", videoId);

                    dnet_id id;
                    id.group_id = 0;
                    id.type = 0;
                    session.transform(videoId, id);

                    session.exec(&id, "downloader-queue@push", videoId);
                }
            }

            response()->write("ok");
            response()->close();
        }

        void on_download_failed(int code, const std::string& message)
        {
            COCAINE_LOG_ERROR(app()->m_log, "Download failed: %s [%d]", message, code);
            response()->error(code, message);
        }

        void on_error(int code, const std::string& message)
        {
            std::cerr << "ERROR: " << code << ", " << message << std::endl;
            COCAINE_LOG_ERROR(app()->m_log, "ERROR: %d: %s", code, message);
        }
    };
};

app_context::app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager)
	: application<app_context>(id, service_manager)
{
	// obtain logging facility
	m_log = service_manager->get_system_logger();
	COCAINE_LOG_INFO(m_log, "application start: %s\n", id.c_str());
}

void app_context::initialize()
{
    create_service(m_fetcher, "urlfetch");

	// configure
	//FIXME: replace this with config storage service when it's done
	{
		rapidjson::Document doc;
		m_state = elliptics_client_state::create("config.json", doc);
	}

	// register event handlers
	on_unregistered<on_event>();
}

int main(int argc, char **argv)
{
	return cocaine::framework::worker_t::run<app_context>(argc, argv);
}
