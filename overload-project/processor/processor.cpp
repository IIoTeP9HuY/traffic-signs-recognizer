#include <cocaine/framework/logging.hpp>
#include <cocaine/framework/application.hpp>
#include <cocaine/framework/worker.hpp>
#include <grape/elliptics_client_state.hpp>
#include <boost/filesystem/operations.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/nonfree/features2d.hpp>

#include <thread>

using namespace ioremap::elliptics;
using namespace cv;

class app_context : public cocaine::framework::application<app_context>
{
public:
	// proxy to the logging service
	std::shared_ptr<cocaine::framework::logger_t> m_log;
    Mat m_example_object;

	// elliptics client generator
	elliptics_client_state m_state;

	app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager);
	void initialize();

    struct on_event :
            public cocaine::framework::handler<app_context>,
            public std::enable_shared_from_this<on_event>
    {
        on_event(std::shared_ptr<app_context> app) : cocaine::framework::handler<app_context>(app)
        {
            // pass
        }

        void on_chunk(const char *chunk, size_t size)
        {
            using namespace std::placeholders;

            exec_context context(data_pointer::copy(chunk, size));
            COCAINE_LOG_INFO(app()->m_log, "on_chunk: %s, data: \"%s\"", context.event(), context.data().to_string());

            std::string token = context.data().to_string();

            if (app()->m_example_object.empty()) {
                COCAINE_LOG_INFO(app()->m_log, "Reading example image");

                std::string example_path = "example_image.jpeg";

                session sess = app()->m_state.create_session();
                sess.set_namespace("youtube_images", 7 + 1 + 6);
                sess.read_data(example_path, 0, 0)
                        .connect(std::bind(&on_event::on_example_loaded,
                                           shared_from_this(),
                                           token, _1, _2));
            } else {
                load_image(token);
            }

//            std::thread thread(&on_event::split_to_images, shared_from_this(), token);
//            thread.detach();
        }

        void on_example_loaded(const std::string &token, const sync_read_result &result, const error_info &error)
        {
            using namespace std::placeholders;

            if (error) {
                COCAINE_LOG_ERROR(app()->m_log, "Failed to read \"%s\" from elliptics: %s",
                                  "example_image.jpeg", error.message());
                response()->error(error.code(), error.message());
                return;
            }

            data_pointer file = result[0].file();

            std::vector<char> input(file.data<char>(), file.data<char>() + file.size());
            input.push_back('\0');
            app()->m_example_object = imdecode(input, 1);

            load_image(token);
        }

        void load_image(const std::string &token)
        {
            using namespace std::placeholders;

            COCAINE_LOG_INFO(app()->m_log, "Reading image %s from elliptics", token);

            session sess = app()->m_state.create_session();
            sess.set_namespace("youtube_images", 7 + 1 + 6);
            sess.read_data(token, 0, 0)
                    .connect(std::bind(&on_event::on_image_loaded,
                                       shared_from_this(),
                                       token, _1, _2));
        }

        void on_image_loaded(const std::string &token, const sync_read_result &result, const error_info &error)
        {
            using namespace std::placeholders;

            if (error) {
                COCAINE_LOG_ERROR(app()->m_log, "Failed to read \"%s\" from elliptics: %s",
                                  token, error.message());
                response()->error(error.code(), error.message());
                return;
            }

            COCAINE_LOG_INFO(app()->m_log, "Finished reading image %s from elliptics", token);

            data_pointer file = result[0].file();

            data_pointer processing_result = process_image(file);

            COCAINE_LOG_INFO(app()->m_log, "Finished image %s processing", token);

            session sess = app()->m_state.create_session();
            sess.set_namespace("processed_images", 9 + 1 + 6);

            sess.write_data(token, processing_result, 0)
                    .connect(std::bind(&on_event::on_result_uploaded,
                                       shared_from_this(),
                                       token, _1, _2));
        }

        void on_result_uploaded(const std::string &token, const sync_write_result &, const error_info &error)
        {
            if (error) {
                COCAINE_LOG_ERROR(app()->m_log, "Failed to upload \"%s\" to elliptics: %s",
                                  token, error.message());
                response()->error(error.code(), error.message());
                return;
            }

            COCAINE_LOG_INFO(app()->m_log, "Finished image %s uploading to elliptics", token);

            session sess = app()->m_state.create_session();

            dnet_id id;
            id.group_id = 0;
            id.type = 0;
            sess.transform(token, id);

            sess.exec(&id, "results-queue@push", token);

            response()->write("ok");
            response()->close();
        }

        Mat find_match(Mat img_object, Mat img_scene)
        {
            //-- Step 1: Detect the keypoints using SURF Detector
            int minHessian = 400;

            SurfFeatureDetector detector( minHessian );

            std::vector<KeyPoint> keypoints_object, keypoints_scene;

            detector.detect( img_object, keypoints_object );
            detector.detect( img_scene, keypoints_scene );

            //-- Step 2: Calculate descriptors (feature vectors)
            SurfDescriptorExtractor extractor;

            Mat descriptors_object, descriptors_scene;

            extractor.compute( img_object, keypoints_object, descriptors_object );
            extractor.compute( img_scene, keypoints_scene, descriptors_scene );

            //-- Step 3: Matching descriptor vectors using FLANN matcher
            FlannBasedMatcher matcher;
            std::vector< DMatch > matches;
            matcher.match( descriptors_object, descriptors_scene, matches );

            double max_dist = 0; double min_dist = 100;

            //-- Quick calculation of max and min distances between keypoints
            for ( int i = 0; i < descriptors_object.rows; i++ )
            {
                double dist = matches[i].distance;
                if ( dist < min_dist ) min_dist = dist;
                if ( dist > max_dist ) max_dist = dist;
            }

            printf("-- Max dist : %f \n", max_dist );
            printf("-- Min dist : %f \n", min_dist );

            //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
            std::vector< DMatch > good_matches;

            for ( int i = 0; i < descriptors_object.rows; i++ )
            {
                if ( matches[i].distance < 3 * min_dist )
                {
                    good_matches.push_back( matches[i]);
                }
            }

            Mat img_matches;
            drawMatches( img_object, keypoints_object, img_scene, keypoints_scene,
                         good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                         vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );


            //-- Localize the object from img_1 in img_2
            std::vector<Point2f> obj;
            std::vector<Point2f> scene;

            for ( size_t i = 0; i < good_matches.size(); i++ )
            {
                //-- Get the keypoints from the good matches
                obj.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
                scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
            }

            Mat H = findHomography( obj, scene, RANSAC );

            //-- Get the corners from the image_1 ( the object to be "detected" )
            std::vector<Point2f> obj_corners(4);
            obj_corners[0] = Point(0, 0); obj_corners[1] = Point( img_object.cols, 0 );
            obj_corners[2] = Point( img_object.cols, img_object.rows ); obj_corners[3] = Point( 0, img_object.rows );
            std::vector<Point2f> scene_corners(4);

            perspectiveTransform( obj_corners, scene_corners, H);


            //-- Draw lines between the corners (the mapped object in the scene - image_2 )
            Point2f offset( (float)img_object.cols, 0);
            line( img_matches, scene_corners[0] + offset, scene_corners[1] + offset, Scalar(0, 255, 0), 4 );
            line( img_matches, scene_corners[1] + offset, scene_corners[2] + offset, Scalar( 0, 255, 0), 4 );
            line( img_matches, scene_corners[2] + offset, scene_corners[3] + offset, Scalar( 0, 255, 0), 4 );
            line( img_matches, scene_corners[3] + offset, scene_corners[0] + offset, Scalar( 0, 255, 0), 4 );

            //-- Show detected matches
            return img_matches;
            // imshow( "Good Matches & Object detection", img_matches );
            // imsave( "Good Matches & Object detection", img_matches );

            waitKey(0);
        }

        data_pointer process_image(const data_pointer &jpeg_data)
        {
            std::vector<char> input(jpeg_data.data<char>(), jpeg_data.data<char>() + jpeg_data.size());
            input.push_back('\0');
            Mat img_scene = imdecode(input, 1);

            Mat match = find_match(app()->m_example_object, img_scene);

            std::vector<uchar> result;
            imencode(".jpg", match, result);
            return data_pointer::copy(result.data(), result.size());
        }

        void on_error(int code, const std::string& message)
        {
            std::cerr << "ERROR: " << code << ", " << message << std::endl;
            COCAINE_LOG_ERROR(app()->m_log, "ERROR: %d: %s", code, message);
        }
    };
};

app_context::app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager)
	: application<app_context>(id, service_manager)
{
	// obtain logging facility
	m_log = service_manager->get_system_logger();
	COCAINE_LOG_INFO(m_log, "application start: %s\n", id.c_str());
}

void app_context::initialize()
{
	// configure
	//FIXME: replace this with config storage service when it's done
	{
		rapidjson::Document doc;
		m_state = elliptics_client_state::create("config.json", doc);
	}

	// register event handlers
	on_unregistered<on_event>();
}

int main(int argc, char **argv)
{
	return cocaine::framework::worker_t::run<app_context>(argc, argv);
}
