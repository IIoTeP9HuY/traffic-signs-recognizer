#include <cocaine/framework/logging.hpp>
#include <cocaine/framework/application.hpp>
#include <cocaine/framework/worker.hpp>
#include <grape/elliptics_client_state.hpp>

#include <thread>

using namespace ioremap::elliptics;

class app_context : public cocaine::framework::application<app_context>
{
public:
	// proxy to the logging service
	std::shared_ptr<cocaine::framework::logger_t> m_log;

	// elliptics client generator
	elliptics_client_state m_state;

	app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager);
	void initialize();

    struct on_event :
            public cocaine::framework::handler<app_context>,
            public std::enable_shared_from_this<on_event>
    {
        on_event(std::shared_ptr<app_context> app) : cocaine::framework::handler<app_context>(app)
        {
            // pass
        }

        void on_chunk(const char *chunk, size_t size)
        {
            using namespace std::placeholders;

            exec_context context(data_pointer::copy(chunk, size));
            COCAINE_LOG_INFO(app()->m_log, "on_chunk: %s, data: \"%s\"", context.event(), context.data().to_string());

            std::string token = context.data().to_string();

            session sess = app()->m_state.create_session();
            sess.set_namespace("youtube", 7);
            sess.lookup(token)
                    .connect(std::bind(&on_event::on_lookup_finished,
                                       shared_from_this(),
                                       std::placeholders::_2,
                                       token));
        }

        void on_lookup_finished(const error_info &err, const std::string &token)
        {
            if (err) {
                // Video is not downloaded yet
                std::thread thread(&on_event::download, shared_from_this(), token);
                thread.detach();
            } else {
                COCAINE_LOG_INFO(app()->m_log, "Video already downloaded: %s", token);
                // Video is already downloaded, resplit and reprocess it
                on_finished(token);
            }
        }

        void on_finished(const std::string &token)
        {
            session sess = app()->m_state.create_session();

            dnet_id id;
            id.group_id = 0;
            id.type = 0;
            sess.transform(token, id);

            sess.exec(&id, "splitter-queue@push", token);

            response()->write("ok");
            response()->close();
        }

        static void download(std::shared_ptr<on_event> that, std::string token)
        {
            COCAINE_LOG_INFO(that->app()->m_log, "Started downloading the video: %s", token);

            std::string command = cocaine::format("/usr/bin/youtube-dl -o /tmp/%s -f 18/22/37 %s", token, token);
            int err = system(command.c_str());

            if (err) {
                COCAINE_LOG_ERROR(that->app()->m_log, "Failed to run youtube-dl, err: %d", err);
                that->response()->error(err, "Failed to run youtube-dl");
                return;
            }

            COCAINE_LOG_INFO(that->app()->m_log, "Finished downloading the video: %s", token);

            try {
                session sess = that->app()->m_state.create_session();
                sess.set_namespace("youtube", 7);
                sess.write_file(token, "/tmp/" + token, 0, 0, 0);
            } catch (error &e) {
                COCAINE_LOG_ERROR(that->app()->m_log, "Failed to write video to elliptics: %s", e.error_message());
                that->response()->error(e.error_code(), e.error_message());
                return;
            }

            std::string filepath = cocaine::format("/tmp/%s", token);
            err = unlink(filepath.c_str());
            if (err) {
                err = errno;
                COCAINE_LOG_ERROR(that->app()->m_log, "Temporary file removing result: %d", err);
            }

            that->on_finished(token);
        }

        void on_error(int code, const std::string& message)
        {
            std::cerr << "ERROR: " << code << ", " << message << std::endl;
            COCAINE_LOG_ERROR(app()->m_log, "ERROR: %d: %s", code, message);
        }
    };
};

app_context::app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager)
	: application<app_context>(id, service_manager)
{
	// obtain logging facility
	m_log = service_manager->get_system_logger();
	COCAINE_LOG_INFO(m_log, "application start: %s\n", id.c_str());
}

void app_context::initialize()
{
	// configure
	//FIXME: replace this with config storage service when it's done
	{
		rapidjson::Document doc;
		m_state = elliptics_client_state::create("config.json", doc);
	}

	// register event handlers
	on_unregistered<on_event>();
}

int main(int argc, char **argv)
{
	return cocaine::framework::worker_t::run<app_context>(argc, argv);
}
