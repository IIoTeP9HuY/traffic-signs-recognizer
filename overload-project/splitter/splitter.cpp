#include <cocaine/framework/logging.hpp>
#include <cocaine/framework/application.hpp>
#include <cocaine/framework/worker.hpp>
#include <grape/elliptics_client_state.hpp>
#include <boost/filesystem/operations.hpp>

#include <thread>

using namespace ioremap::elliptics;

class app_context : public cocaine::framework::application<app_context>
{
public:
	// proxy to the logging service
	std::shared_ptr<cocaine::framework::logger_t> m_log;

	// elliptics client generator
	elliptics_client_state m_state;

	app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager);
	void initialize();

    struct on_event :
            public cocaine::framework::handler<app_context>,
            public std::enable_shared_from_this<on_event>
    {
        on_event(std::shared_ptr<app_context> app) : cocaine::framework::handler<app_context>(app)
        {
            // pass
        }

        void on_chunk(const char *chunk, size_t size)
        {
            using namespace std::placeholders;

            exec_context context(data_pointer::copy(chunk, size));
            COCAINE_LOG_INFO(app()->m_log, "on_chunk: %s, data: \"%s\"", context.event(), context.data().to_string());

            std::string token = context.data().to_string();

            std::thread thread(&on_event::split_to_images, shared_from_this(), token);
            thread.detach();
        }

        static void split_to_images(std::shared_ptr<on_event> that, std::string token)
        {
            const std::string directory = cocaine::format("/tmp/%s_images", token);
            const std::string video = cocaine::format("/tmp/%s_video", token);

            try {
                session sess = that->app()->m_state.create_session();
                sess.set_namespace("youtube", 7);
                sess.read_file(token, video, 0, 0);
            } catch (error &e) {
                COCAINE_LOG_ERROR(that->app()->m_log, "Failed to read video from elliptics: %s", e.error_message());
                that->response()->error(e.error_code(), e.error_message());
                return;
            }

            int err = mkdir(directory.c_str(), 0777);
            if (err) {
                err = errno;

                if (err != EEXIST) {
                    COCAINE_LOG_ERROR(that->app()->m_log, "Failed to mkdir %s, err: %d", directory, -err);
                    that->response()->error(err, "Failed to mkdir");
                    return;
                }
            }

            const std::string command = cocaine::format("ffmpeg -i %s -r 1/2 %s/%%05d.jpg", video, directory);
            err = system(command.c_str());

            if (err) {
                rmdir(directory.c_str());
                COCAINE_LOG_ERROR(that->app()->m_log, "Failed to run ffmpeg, err: %d", err);
                that->response()->error(err, "Failed to run youtube-dl");
                return;
            }

            COCAINE_LOG_INFO(that->app()->m_log, "Finished splitting the video");

            session sess = that->app()->m_state.create_session();
            sess.set_namespace("youtube_images", 7 + 1 + 6);

            boost::filesystem3::directory_iterator it(directory), end;
            for (int i = 0; it != end; ++i) {
                auto path = boost::filesystem3::absolute(it->path(), directory);
                std::string image_id = cocaine::format("%s-%d.jpeg", token, i);
                ++it;

                COCAINE_LOG_INFO(that->app()->m_log, "Writing file %s to elliptics", path);
                try {
                    sess.write_file(image_id, path.string(), 0, 0, 0);
                } catch (error &e) {
                    COCAINE_LOG_ERROR(that->app()->m_log, "Failed to write image \"%s\" to elliptics: %s", path.string(), e.error_message());
                }

                dnet_id id;
                id.group_id = 0;
                id.type = 0;
                sess.transform(image_id, id);

                sess.exec(&id, "processor-queue@push", image_id);

                boost::filesystem3::remove(path);
            }

            boost::filesystem3::remove(video);
            boost::filesystem3::remove(directory);


            that->response()->write("ok");
            that->response()->close();
        }

        void on_error(int code, const std::string& message)
        {
            std::cerr << "ERROR: " << code << ", " << message << std::endl;
            COCAINE_LOG_ERROR(app()->m_log, "ERROR: %d: %s", code, message);
        }
    };
};

app_context::app_context(const std::string &id, std::shared_ptr<cocaine::framework::service_manager_t> service_manager)
	: application<app_context>(id, service_manager)
{
	// obtain logging facility
	m_log = service_manager->get_system_logger();
	COCAINE_LOG_INFO(m_log, "application start: %s\n", id.c_str());
}

void app_context::initialize()
{
	// configure
	//FIXME: replace this with config storage service when it's done
	{
		rapidjson::Document doc;
		m_state = elliptics_client_state::create("config.json", doc);
	}

	// register event handlers
	on_unregistered<on_event>();
}

int main(int argc, char **argv)
{
	return cocaine::framework::worker_t::run<app_context>(argc, argv);
}
