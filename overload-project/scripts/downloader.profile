{
    "heartbeat-timeout" : 60,
    "pool-limit" : 1,
    "queue-limit" : 10,
    "grow-threshold" : 1,
    "concurrency" : 1
}
