{
    "heartbeat-timeout" : 60,
    "pool-limit" : 1,
    "queue-limit" : 1024,
    "idle-timeout" : 0,
    "grow-threshold" : 1,
    "concurrency" : 10
}
