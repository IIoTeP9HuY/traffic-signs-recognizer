#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

/** Function Headers */
void detectAndDisplay( Mat frame );

/** Global variables */
String sign_cascade_name = "cascade.xml";
CascadeClassifier sign_cascade;
string window_name = "Capture - Sign detection";
RNG rng(12345);

/** @function main */
int main( int argc, const char **argv )
{
    Mat image = imread( argv[1] );
    Mat frame;

    //-- 1. Load the cascades
    if ( !sign_cascade.load( sign_cascade_name ) )
    {
        printf("--(!)Error loading sign cascade\n");
        return -1;
    };

    while ( true )
    {
        frame = image;
        //-- 3. Apply the classifier to the frame
        if ( !frame.empty() )
        {
            detectAndDisplay( frame );
        }
        else
        {
            printf(" --(!) No captured frame -- Break!");
            break;
        }

        int c = waitKey(100000);
        if ( (char)c == 'c' )
        {
            break;
        }
    }
    return 0;
}

/** @function detectAndDisplay */
void detectAndDisplay( Mat frame )
{
    std::vector<Rect> faces;
    Mat frame_gray;

    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );

    //-- Detect faces
    sign_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30) );

    for ( int i = 0; i < faces.size(); i++ )
    {
        Point center( faces[i].x + faces[i].width * 0.5, faces[i].y + faces[i].height * 0.5 );
        ellipse( frame, center, Size( faces[i].width * 0.5, faces[i].height * 0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );

        Mat faceROI = frame_gray( faces[i] );
        std::vector<Rect> eyes;

        // //-- In each face, detect eyes
        // eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

        // for( int j = 0; j < eyes.size(); j++ )
        //  {
        //    Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
        //    int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
        //    circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
        //  }
    }
    //-- Show what you got
    imshow( window_name, frame );
}